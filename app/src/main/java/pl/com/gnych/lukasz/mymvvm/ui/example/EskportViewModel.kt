package pl.com.gnych.lukasz.mymvvm.ui.example

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.view.View
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import pl.com.gnych.lukasz.mymvvm.R
import pl.com.gnych.lukasz.mymvvm.models.EksportDao
import pl.com.gnych.lukasz.mymvvm.models.eksport.Eksport

class EskportViewModel() : ViewModel() {

    val loadingVisibility: MutableLiveData<Int> = MutableLiveData()
    val eksportAdapter: EksportAdapter = EksportAdapter()
    val errorMessage: MutableLiveData<Int> = MutableLiveData()
    val errorClickListener = View.OnClickListener { loadExampleData() }
    private lateinit var eksportDao: EksportDao


    private val disposable = CompositeDisposable()

    fun setEksportDao(eksportDao: EksportDao) {
        this.eksportDao = eksportDao
    }

    fun loadExampleData() {
        eksportDao.insert(Eksport(1, "Example", System.currentTimeMillis(), "30.01", "12.15", "Tomek1", "Lokal"))
        eksportDao.insert(Eksport(2, "Example1", System.currentTimeMillis(), "31.01", "12.25", "Tomek2", "Loka3"))
        eksportDao.insert(Eksport(3, "Example2", System.currentTimeMillis(), "32.01", "12.75", "Tomek3", "Lokal"))
        Observable.fromCallable { eksportDao.all }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { onRetrieveDataStart() }
                .doOnTerminate { onRetrieveFinish() }
                .subscribe(
                        { result -> onRetrieveSuccess(result) },
                        { onRetrieveError() }
                ).addTo(disposable)
    }

    fun onRetrieveDataStart() {
        loadingVisibility.value = View.VISIBLE
        errorMessage.value = null
    }

    fun onRetrieveFinish() {
        loadingVisibility.value = View.GONE
    }

    fun onRetrieveSuccess(listPosts: List<Eksport>) {
        eksportAdapter.updateEksports(listPosts)
    }

    fun onRetrieveError() {
        errorMessage.value = R.string.generic_error
    }

    override fun onCleared() {
        super.onCleared()
        disposable.dispose()
    }

    fun getEksportsByName(name: String) {
        eksportAdapter.updateEksports(eksportDao.byName(name).orEmpty())
    }

    fun getEksportsByUserName(userName: String) {
        eksportAdapter.updateEksports(eksportDao.byUserName(userName).orEmpty())
    }

    fun getEksportsByLocalName(localName: String) {
        eksportAdapter.updateEksports(eksportDao.byLocal(localName).orEmpty())
    }

    fun getEksportByDate(time: Long) {
        //todo
    }

    fun getEksportByHour(time: Long) {
        //todo
    }
}