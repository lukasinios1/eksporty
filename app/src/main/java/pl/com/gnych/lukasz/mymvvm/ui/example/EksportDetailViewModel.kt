package pl.com.gnych.lukasz.mymvvm.ui.example

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import pl.com.gnych.lukasz.mymvvm.models.eksport.Eksport

class EksportDetailViewModel : ViewModel() {

    private val id = MutableLiveData<Long>()
    private val name = MutableLiveData<String>()
    private val date = MutableLiveData<String>()
    private val hour = MutableLiveData<String>()
    private val userName = MutableLiveData<String>()
    private val localName = MutableLiveData<String>()


    fun bindEksport(eksport: Eksport) {
        this.id.value = eksport.id
        this.name.value = eksport.name
        this.date.value = eksport.date
        this.hour.value = eksport.hour
        this.userName.value = eksport.userName
        this.localName.value = eksport.localName
    }

    fun getId(): MutableLiveData<Long> = id

    fun getName(): MutableLiveData<String> = name

    fun getDate(): MutableLiveData<String> = date

    fun getHour(): MutableLiveData<String> = hour

    fun getUserName(): MutableLiveData<String> = userName

    fun getLocalName(): MutableLiveData<String> = localName
}