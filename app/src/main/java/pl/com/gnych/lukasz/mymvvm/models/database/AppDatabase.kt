package pl.com.gnych.lukasz.mymvvm.models.database

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import pl.com.gnych.lukasz.mymvvm.models.EksportDao
import pl.com.gnych.lukasz.mymvvm.models.eksport.Eksport

@Database(
        entities = [Eksport::class],
    version = 1,
    exportSchema = false
)
abstract class AppDatabase : RoomDatabase() {
    abstract fun postEksport(): EksportDao
}