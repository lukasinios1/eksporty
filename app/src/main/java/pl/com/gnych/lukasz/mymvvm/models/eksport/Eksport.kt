package pl.com.gnych.lukasz.mymvvm.models.eksport

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity
data class Eksport(
        @PrimaryKey(autoGenerate = true)
        var id: Long = 0,
        var name: String? = null,
        var dateLong: Long? = null,
        var date: String? = null,
        var hour: String? = null,
        var userName: String? = null,
        var localName: String? = null
)