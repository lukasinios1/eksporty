package pl.com.gnych.lukasz.mymvvm.models

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy.REPLACE
import android.arch.persistence.room.Query
import pl.com.gnych.lukasz.mymvvm.models.eksport.Eksport

@Dao
interface EksportDao {
    @get:Query("SELECT * FROM Eksport")
    val all: List<Eksport>

    @Insert
    fun insertAll(countries: List<Eksport>)

    @Insert(onConflict = REPLACE)
    fun insert(eksport: Eksport)

    @Query("SELECT * FROM Eksport WHERE name LIKE '%' || :name || '%'")
    fun byName(name: String): List<Eksport>?

    @Query("SELECT * FROM Eksport WHERE localName LIKE '%' || :localName || '%'")
    fun byLocal(localName: String): List<Eksport>?

    @Query("SELECT * FROM Eksport WHERE userName LIKE '%' || :userName || '%'")
    fun byUserName(userName: String): List<Eksport>?

}