package pl.com.gnych.lukasz.mymvvm.ui.example

import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import pl.com.gnych.lukasz.mymvvm.R
import pl.com.gnych.lukasz.mymvvm.databinding.EksportItemBinding
import pl.com.gnych.lukasz.mymvvm.models.eksport.Eksport

class EksportAdapter : RecyclerView.Adapter<EksportAdapter.ViewHolder>() {

    private lateinit var eksports: List<Eksport>

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): EksportAdapter.ViewHolder =
            ViewHolder(DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.eksport_item, parent, false))

    override fun getItemCount(): Int = if (::eksports.isInitialized) eksports.size else 0

    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.bind(eksports[position])

    fun updateEksports(eskportLists: List<Eksport>) {
        this.eksports = eskportLists
        notifyDataSetChanged()
    }

    class ViewHolder(private val binding: EksportItemBinding) : RecyclerView.ViewHolder(binding.root) {

        private val viewModel = EksportDetailViewModel()
        fun bind(eksport: Eksport) {
            viewModel.bindEksport(eksport)
            binding.viewModel = viewModel
        }
    }

}
