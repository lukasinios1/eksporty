package pl.com.gnych.lukasz.mymvvm.ui.example

import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.arch.lifecycle.ViewModelProviders
import android.arch.persistence.room.Room
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.widget.Button
import android.widget.EditText
import kotlinx.android.synthetic.main.activity_main.*
import pl.com.gnych.lukasz.mymvvm.R
import pl.com.gnych.lukasz.mymvvm.databinding.ActivityMainBinding
import pl.com.gnych.lukasz.mymvvm.models.database.AppDatabase
import java.util.*


class EksportActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private lateinit var viewModel: EskportViewModel
    private var searchString: String = ""
    private var type: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        supportActionBar?.hide()
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        binding.postList.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)

        viewModel = ViewModelProviders.of(this).get(EskportViewModel::class.java)
        viewModel.setEksportDao(Room.databaseBuilder(this, AppDatabase::class.java, "eksport-database")
                .allowMainThreadQueries()
                .build()
                .postEksport())
        binding.viewModel = viewModel

        val dialogBuilder = AlertDialog.Builder(this).create()
        val inflater = this.layoutInflater
        val dialogView = inflater.inflate(R.layout.dialog, null)

        val editText = dialogView.findViewById(R.id.edt_comment) as EditText
        val button1 = dialogView.findViewById(R.id.buttonSubmit) as Button
        val button2 = dialogView.findViewById(R.id.buttonCancel) as Button

        button2.setOnClickListener {
            dialogBuilder.dismiss()
        }
        button1.setOnClickListener {
            searchString = editText.text.toString()
            editText.setText("")
            dialogBuilder.dismiss()
            when (type) {
                1 -> viewModel.getEksportsByName(searchString)
                2 -> viewModel.getEksportsByUserName(searchString)
                3 -> viewModel.getEksportsByLocalName(searchString)
            }
        }

        dialogBuilder.setView(dialogView)

        name.setOnClickListener {
            dialogBuilder.show()
            type = 1
        }
        date.setOnClickListener {
            DatePickerDialog(this, DatePickerDialog.OnDateSetListener { _, year, month, day ->
                val calendar: Calendar = Calendar.getInstance()
                calendar.set(year, month, day)
                viewModel.getEksportByDate(calendar.timeInMillis)
            }, Calendar.getInstance().get(Calendar.YEAR), Calendar.getInstance().get(Calendar.MONTH), Calendar.getInstance().get(Calendar.DAY_OF_MONTH)).show()
        }
        hour.setOnClickListener {
            TimePickerDialog(this, TimePickerDialog.OnTimeSetListener { _, hour, minute ->
                val calendar: Calendar = Calendar.getInstance()
                calendar.set(0, 0, 0, hour, minute)
                viewModel.getEksportByHour(calendar.timeInMillis)
            }, Calendar.getInstance().get(Calendar.HOUR_OF_DAY), Calendar.getInstance().get(Calendar.MINUTE), true).show()
        }
        user.setOnClickListener {
            dialogBuilder.show()
            type = 2
        }
        local.setOnClickListener {
            dialogBuilder.show()
            type = 3
        }
        viewModel.loadExampleData()
    }
}